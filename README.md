原网页被屏蔽，您已被自动跳转到镜像网址列表。

[![博讯新闻](https://bitbucket.org/greatfire/test/raw/master/boxun.png "博讯新闻 - 免翻墙镜像") **博讯新闻**](https://boxun10.azurewebsites.net "博讯新闻 - 免翻墙镜像") | [![人民监督网](https://bitbucket.org/greatfire/test/raw/master/renminjianduwang.png "人民监督网 - 免翻墙镜像") **人民监督网**](https://rmjdw7.global.ssl.fastly.net/ "人民监督网 - 免翻墙镜像")
------------- | -------------
[![Google](https://bitbucket.org/greatfire/test/raw/master/google.png "Google - 免翻墙镜像") **Google**](https://e127f.azurewebsites.net "Google - 免翻墙镜像") | [![中国数字时代](https://bitbucket.org/greatfire/test/raw/master/cdt.png "中国数字时代 - 免翻墙镜像") **中国数字时代**](https://cdt2.azurewebsites.net "中国数字时代 - 免翻墙镜像")
[![德国之声](https://bitbucket.org/greatfire/test/raw/master/dw.png "德国之声 - 免翻墙镜像") **德国之声**](https://welle.global.ssl.fastly.net/ "德国之声 - 免翻墙镜像") | [![泡泡](https://bitbucket.org/greatfire/test/raw/master/paopao.png "泡泡 - 未经审查的互联网信息 - 免翻墙镜像") **泡泡**](https://pp5.global.ssl.fastly.net/ "泡泡 - 未经审查的互联网信息 - 免翻墙镜像")
[![自由微博](https://bitbucket.org/greatfire/test/raw/master/freeweibo.png "自由微博 - 匿名和不受屏蔽的新浪微博搜索 - 免翻墙镜像") **自由微博**](https://fw3.global.ssl.fastly.net/ "自由微博 - 匿名和不受屏蔽的新浪微博搜索 - 免翻墙镜像") | [![蓝灯/Lantern](https://bitbucket.org/greatfire/test/raw/master/lantern.png "以及自由微博和GreatFire.org官方中文论坛 - 免翻墙镜像") **蓝灯/Lantern**](https://lantern6.azurewebsites.net "以及自由微博和GreatFire.org官方中文论坛 - 免翻墙镜像")
[![编程随想](https://bitbucket.org/greatfire/test/raw/master/programthink.png "编程随想的博客 - 免翻墙镜像") **编程随想**](https://pt5.global.ssl.fastly.net/ "编程随想的博客 - 免翻墙镜像") | [![BBC 中文](https://bitbucket.org/greatfire/test/raw/master/bbc.png "BBC 中文 - 免翻墙镜像") **BBC 中文**](https://bbc3.global.ssl.fastly.net/ "BBC 中文 - 免翻墙镜像")

